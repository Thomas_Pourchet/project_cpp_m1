#pragma once

#include <algorithm>
#include <numeric>
#include <array>
#include <cassert>
#include <cmath>
#include <iostream>


//Task 4 obj 2

template<typename T, int Size>
class Point{
    public :
        std::array<T, Size> values = {};

        Point() {}
        Point(T x, T y) : values { x, y } {static_assert(Size == 2);}
        Point(T x, T y, T z) : values { x, y, z } {static_assert(Size == 3);}

        T& x() { return values[0]; }
        T x() const { return values[0]; }

        T& y() { static_assert(Size >= 2); return values[1];}
        T y() const { static_assert(Size >= 2); return values[1]; }

        T& z() { static_assert(Size >= 3); return values[2]; }
        T z() const { static_assert(Size >= 3); return values[2]; }

        Point& operator+=(const Point& other)
        {
            std::transform(values.begin(), values.end(), other.values.begin(), values.begin(), std::plus<T>());
            return *this;
        }

        Point& operator-=(const Point& other)
        {
            std::transform(values.begin(), values.end(), other.values.begin(), values.begin(), std::minus<T>());
            return *this;
        }

        Point& operator*=(const Point& other)
        {
        std::transform(values.begin(), values.end(), other.values.begin(), values.begin(), std::multiplies<T>());
            return *this;
        }

        Point& operator*=(const T scalar)
        {
        std::transform(values.begin(), values.end(), values.begin(), [scalar](T value){return value * scalar;});
            return *this;
        }

        Point operator+(const Point& other) const
        {
            Point result = *this;
            result += other;
            return result;
        }

        Point operator-(const Point& other) const
        {
            Point result = *this;
            result -= other;
            return result;
        }

        Point operator*(const Point& other) const{
            Point result = *this;
            result *= other;
            return result;
        }

        Point operator*(const T scalar) const
        {
            Point result = *this;
            result *= scalar;
            return result;
        }

        Point operator-() const { return Point { -x(), -y(), -z() }; }

        T length() const { return std::sqrt(std::reduce(values.begin(), values.end(), static_cast<T>(0), [](T acc, T value) { return acc + (value * value); }));}

        T distance_to(const Point& other) const { return (*this - other).length(); }

        Point& normalize(const float target_len = 1.0f)
        {
            const float current_len = length();
            if (current_len == 0)
            {
                throw std::logic_error("cannot normalize vector of length 0");
            }

            *this *= (target_len / current_len);
            return *this;
        }

        Point& cap_length(const T max_len)
        {
            assert(max_len > 0);

            const float current_len = length();
            if (current_len > max_len)
            {
                *this *= (max_len / current_len);
            }

            return *this;
        }
    };

// our 3D-coordinate system will be tied to the airport: the runway is parallel to the x-axis, the z-axis
// points towards the sky, and y is perpendicular to both thus,
// {1,0,0} --> {.5,.5}   {0,1,0} --> {-.5,.5}   {0,0,1} --> {0,1}



inline void test_generic_points(){
    Point<float, 3> p1 = {1, 2, 4};
    Point<float, 3> p2 = {1, 2, 3};
    auto p3 = p1 + p2;
    p1 += p2;
    p3 *= 3.f; // ou 3.f, ou 3.0 en fonction du type de Point
}


using Point2D = Point<float, 2>;
using Point3D = Point<float, 3>;

inline Point2D project_2D(const Point3D& p)
{
    return { .5f * p.x() - .5f * p.y(), .5f * p.x() + .5f * p.y() + p.z() };
}
