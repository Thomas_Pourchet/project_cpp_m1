#pragma once

#include "GL/displayable.hpp"
#include "GL/dynamic_object.hpp"
#include "GL/texture.hpp"
#include "airport_type.hpp"
#include "geometry.hpp"
#include "img/image.hpp"
#include "runway.hpp"
#include "terminal.hpp"
#include "tower.hpp"
#include "AircraftFactory.hpp"

#include <vector>
#include <iostream>
#include <memory>



class AircraftManager : public GL::DynamicObject{
    
    private :
        std::vector<std::unique_ptr<Aircraft>> aircrafts;
        AircraftFactory aircraft_factory;
        int nb_crash = 0;
    
    public :
        void add_aircraft(Airport& airport);
        void move() override;
        void printNbAircraft(int index) const;
        int get_required_fuel() const;
        void total_crash() const;
};