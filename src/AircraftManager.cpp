#include "AircraftManager.hpp"
#include "AircraftFactory.hpp"
#include "aircraft.hpp"


void AircraftManager::add_aircraft(Airport& airport)
{
    aircrafts.emplace_back(std::move(aircraft_factory.create_random_aircraft(airport)));
}

void AircraftManager::printNbAircraft(int index) const {
    assert(index > 1 && index < 8);
    auto airline = aircraft_factory.getAirline(index);
    // recherche si airline est dans le nom de l'avion pour chaque condition validé il incrémente le compteur par défaut 0
    std::cout << "Il y a " << std::count_if(aircrafts.begin(), aircrafts.end(), [airline](auto& aircraft){return aircraft->get_flight_num().find(airline) != std::string::npos ;}) << " avion de type " << airline << std::endl;
}

//task 2 - D - 2

int AircraftManager::get_required_fuel() const {
    //garde en mémoire un accumulateur qui augmente si la condition est respectée
    return std::accumulate(aircrafts.begin(), aircrafts.end(), 0, [](auto acc, const auto& aircraft){
        if(!aircraft->is_passed_terminal() && aircraft->is_low_on_fuel()){
            return acc + 3000 - aircraft->getFuel();
        }
        return acc;
    });
}

//Task 3 - OBJ 1 - 2
void AircraftManager::total_crash() const {
    std::cout << "There was " <<  nb_crash << " crash in all" << std::endl;
}

void AircraftManager::move() {

    //task 2 - C

    std::sort(aircrafts.begin(), aircrafts.end(), [] (const std::unique_ptr<Aircraft>& a,const std::unique_ptr<Aircraft>& b){ //& pour ne pas copier le pointeur
        //Prédicat qui test d'abord si un des deux sont dans un terminal puis le fuel
        
        if( b->has_terminal() ){
            return false;
        }
        
        if(a->has_terminal()){
            return true;
        }

        return a->getFuel() < b->getFuel();
    });

    
    // aircraft->update sert de prédicat
    aircrafts.erase(std::remove_if(aircrafts.begin(), aircrafts.end(), [this](auto& aircraft){
        assert(&aircraft);
        
        try{
            return !aircraft->update();
        }
        
        catch(const AircraftCrash& err){
            std::cerr << err.what() << std::endl; //affiche ll'erreur remonter et efface l'avion en renvoyant true
            nb_crash += 1;
            return true;
        }
        
        
        }), aircrafts.end()); //remove_if met ceux à effacer après le dernier élément
}