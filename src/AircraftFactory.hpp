#pragma once

#include "GL/displayable.hpp"
#include "GL/dynamic_object.hpp"
#include "GL/texture.hpp"
#include "airport_type.hpp"
#include "geometry.hpp"
#include "img/image.hpp"
#include "runway.hpp"
#include "terminal.hpp"
#include "tower.hpp"

#include <vector>
#include <iostream>
#include <memory>
#include <functional>

constexpr size_t NUM_AIRCRAFT_TYPES = 3;

class AircraftFactory{

    private :
        std::unique_ptr<Aircraft> create_aircraft(const AircraftType& type,  Airport& airport);
        std::unordered_set<std::string> names = {};
        const std::string airlines[8] = { "AF", "LH", "EY", "DL", "KL", "BA", "AY", "EY" };
        AircraftType* aircraft_types[NUM_AIRCRAFT_TYPES] {};
        void init_aircraft_types();
    
    public :
        std::unique_ptr<Aircraft> create_random_aircraft(Airport& airport);
        const std::string& getAirline(int index) const;
        AircraftFactory();

};