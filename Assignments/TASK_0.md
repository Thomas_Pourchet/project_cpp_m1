# Se familiariser avec l'existant

## A- Exécution

Compilez et lancez le programme.

Allez dans le fichier `tower_sim.cpp` et recherchez la fonction responsable de gérer les inputs du programme.
Sur quelle touche faut-il appuyer pour ajouter un avion ?
Comment faire pour quitter le programme ?
A quoi sert la touche 'F' ?

>   C : ajoute un avion \
    Q, x : quitter le programme \
    F : met la fenêtre en grand écran 

Ajoutez un avion à la simulation et attendez.
Que est le comportement de l'avion ?
Quelles informations s'affichent dans la console ?

> L'avion atterit et se dirige vers un terminal puis part loin de l'aéroport


>   AY2417 is now landing... \
    now servicing AY2417 ...   \
    done servicing AY2417 ...  \
    AY2417 lift off

Ajoutez maintenant quatre avions d'un coup dans la simulation.
Que fait chacun des avions ?

> Quand les 3 terminaux sont utilisés le 4eme avion attend autour de l'aéroport que la place se libère pour atterir à son tour

## B- Analyse du code

Listez les classes du programme à la racine du dossier src/.
Pour chacune d'entre elle, expliquez ce qu'elle représente et son rôle dans le programme.



> class Aircraft : classe qui représente un avion et ces coordonnées. elle controle ses mouvements . \
 class Airport : représente un aéroport. Elle controle les terminaux et possède une tour (classe tower) \
 class AirportType : Initialise un aéroport avec les coordonnée des terminaux et des pistes d'atterrissage notamment \
 class Terminal : représente un terminal et controle l'avion qui si pose \
 class Tower : relier à un aéroport, il gère les déplacement des avions et leur réserve les terminaux. \
 class TowerSimulation : Classe lancer par le main elle initialise le programme, contient la boucle principale qui lis sur le clavier \
 class WaypointType : Représente des coordonnées 3D sur x, y, z utilisé par la plupart des autres classes.


Pour les classes `Tower`, `Aircaft`, `Airport` et `Terminal`, listez leurs fonctions-membre publiques et expliquez précisément à quoi elles servent.
Réalisez ensuite un schéma présentant comment ces différentes classes intéragissent ensemble.

> - **class Aircraft :** \
    - get_flight_num = getter qui renvoie le numéro du vol. \
    - distance_to = prend en paramètre un point et renvoie la distance de l'avion par rapport à ce point.
    - display = dessine l'avion sur la fenêtre. \
    - move = déplace l'avion en fonction du chemin donnée par la classe Tower.

> - **class Airport :** \
- get_tower = getter qui renvoie la classe Tower lier à l'aéroport. \
- display = dessine l'aéroport sur la fenêtre. \
- move = applique à tout les terminaux appartenant à l'aéroport la fonction move.


> - **class Terminal :** \
    - in_use = fonction qui renvoie par un boolean l'état d'un terminal c'est à dire assigné à un avion ou vide. \
    - is_servicing = renvoie true si l'avion est garé sinon false\
    - assign_craft = assigne un avion à un terminal. \
    - start_service = l'avion assigné c'est garé dans le terminal. \
    - finish_service = l'avion assigné quitte le terminal. \
    - move = met à jour l'état des avions dans les terminaux. 

> - **class Tower :** \
    - get_instructions = donnent les instructions à un avion, c'est à dire le chemin qu'il doit suivre, soit tourner autour de l'aéroport ou lui assigné un terminal \
    - arrived_at_terminal = retrouve le terminal assigné à l'avion pour l'avertir.

Quelles classes et fonctions sont impliquées dans la génération du chemin d'un avion ?

> La classe Tower avec la fonction get_instructions et get_circle, Airport avec la fonction start_path, AirportType avec terminal_to_air.

Quel conteneur de la librairie standard a été choisi pour représenter le chemin ?
Expliquez les intérêts de ce choix.

> Le conteneur choisi est std::deque. std::deque est ici plus judicieux qu'une simple pile car on peux  modifier les chemins aux cours du temps à la fin ou au début du conteneur.

## C- Bidouillons !

1) Déterminez à quel endroit du code sont définies les vitesses maximales et accélération de chaque avion.
Le Concorde est censé pouvoir voler plus vite que les autres avions.
Modifiez le programme pour tenir compte de cela.

2) Identifiez quelle variable contrôle le framerate de la simulation.
Ajoutez deux nouveaux inputs au programme permettant d'augmenter ou de diminuer cette valeur.
Essayez maintenant de mettre en pause le programme en manipulant ce framerate. Que se passe-t-il ?\
Ajoutez une nouvelle fonctionnalité au programme pour mettre le programme en pause, et qui ne passe pas par le framerate.

> Si on met le framerate à 0 il va faire une division par 0 et donc le programme crash.

3) Identifiez quelle variable contrôle le temps de débarquement des avions et doublez-le.

4) Lorsqu'un avion a décollé, il réattérit peu de temps après.
Faites en sorte qu'à la place, il soit retiré du programme.\
Indices :\
A quel endroit pouvez-vous savoir que l'avion doit être supprimé ?\
Pourquoi n'est-il pas sûr de procéder au retrait de l'avion dans cette fonction ?
A quel endroit de la callstack pourriez-vous le faire à la place ?\
Que devez-vous modifier pour transmettre l'information de la première à la seconde fonction ?

> C'est dans la classe Aircraft qu'on peux savoir si l'avion peux être supprimer car lui seul peux connaitre ca distance par rapport à l'aéroport. Mais on peux que le supprimer dans la boucle de la fonction timer quand on fait appel à move sur les avions.
Move doit renvoyer true ou false.

5) Lorsqu'un objet de type `Displayable` est créé, il faut ajouter celui-ci manuellement dans la liste des objets à afficher.
Il faut également penser à le supprimer de cette liste avant de le détruire.
Faites en sorte que l'ajout et la suppression de `display_queue` soit "automatiquement gérée" lorsqu'un `Displayable` est créé ou détruit.
Pourquoi n'est-il pas spécialement pertinent d'en faire de même pour `DynamicObject` ?

> La classe aircraft est la seul DynamicObject qu'on veux effacer et il y a plus de classe qui hérite de DynamicObject que DisplaybleObject.
ceci serait donc pas très optimisé.



6) La tour de contrôle a besoin de stocker pour tout `Aircraft` le `Terminal` qui lui est actuellement attribué, afin de pouvoir le libérer une fois que l'avion décolle.
Cette information est actuellement enregistrée dans un `std::vector<std::pair<const Aircraft*, size_t>>` (size_t représentant l'indice du terminal).
Cela fait que la recherche du terminal associé à un avion est réalisée en temps linéaire, par rapport au nombre total de terminaux.
Cela n'est pas grave tant que ce nombre est petit, mais pour préparer l'avenir, on aimerait bien remplacer le vector par un conteneur qui garantira des opérations efficaces, même s'il y a beaucoup de terminaux.\
Modifiez le code afin d'utiliser un conteneur STL plus adapté. Normalement, à la fin, la fonction `find_craft_and_terminal(const Aicraft&)` ne devrait plus être nécessaire.

## D- Théorie

1) Comment a-t-on fait pour que seule la classe `Tower` puisse réserver un terminal de l'aéroport ?

> La méthode reserve_terminal dans la classe aéoroport est privé donc aucune autre classe peux y accéder à part Tower car elle est friend avec cette classe

2) En regardant le contenu de la fonction `void Aircraft::turn(Point3D direction)`, pourquoi selon-vous ne sommes-nous pas passer par une réference ?
Pensez-vous qu'il soit possible d'éviter la copie du `Point3D` passé en paramètre ?

> La fonction turn est amenée à modifier la valeur donné en paramètre donc il y aurait des effets de bord avec une référence. Non pas dans ce cas je pense.

## E- Bonus

Le temps qui s'écoule dans la simulation dépend du framerate du programme.
La fonction move() n'utilise pas le vrai temps. Faites en sorte que si.
Par conséquent, lorsque vous augmentez le framerate, la simulation s'exécute plus rapidement, et si vous le diminuez, celle-ci s'exécute plus lentement.

Dans la plupart des jeux ou logiciels que vous utilisez, lorsque le framerate diminue, vous ne le ressentez quasiment pas (en tout cas, tant que celui-ci ne diminue pas trop).
Pour avoir ce type de résultat, les fonctions d'update prennent généralement en paramètre le temps qui s'est écoulé depuis la dernière frame, et l'utilise pour calculer le mouvement des entités.

Recherchez sur Internet comment obtenir le temps courant en C++ et arrangez-vous pour calculer le dt (delta time) qui s'écoule entre deux frames.
Lorsque le programme tourne bien, celui-ci devrait être quasiment égale à 1/framerate.
Cependant, si le programme se met à ramer et que la callback de glutTimerFunc est appelée en retard (oui oui, c'est possible), alors votre dt devrait être supérieur à 1/framerate.

Passez ensuite cette valeur à la fonction `move` des `DynamicObject`, et utilisez-la pour calculer les nouvelles positions de chaque avion.
Vérifiez maintenant en exécutant le programme que, lorsque augmentez le framerate du programme, vous n'augmentez pas la vitesse de la simulation.

Ajoutez ensuite deux nouveaux inputs permettant d'accélérer ou de ralentir la simulation.
